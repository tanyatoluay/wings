<?php
class News extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('news_model');
		$this->load->helper('url_helper');
		$this->load->helper('form');

	}

	public function create(){
		if(!isset($this->session->userdata['logged_in'])){
                $data['message_display'] = 'Signin to view this page!';
                $this->load->view('templates/header');
                $this->load->view('charity/login_form', $data);
                $this->load->view('templates/footer', $data);	 
                return; 
            }
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');
		$this->form_validation->set_rules('location', 'Location', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');

		$data['title'] = "Create News";

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
       		$this->load->view('news/create');
       		$this->load->view('templates/footer');	        

		}else{
			$this->news_model->set_news();
			$this->load->view('templates/header', $data);
       		$this->load->view('news/success');
       		$this->load->view('templates/footer', $data);	 
		}
	}


	public function delete($slug){
				if(!isset($this->session->userdata['logged_in'])){
                $data['message_display'] = 'Signin to view this page!';
                $this->load->view('templates/header', $data);
                $this->load->view('charity/login_form', $data);
                $this->load->view('templates/footer');	        
                return; 
            }
		$this->news_model->delete_news($slug);
			$this->load->view('templates/header');
       		$this->load->view('news/success');
       		$this->load->view('templates/footer');	 

	}






	public function edit($slug = NULL){
				if(!isset($this->session->userdata['logged_in'])){
                $data['message_display'] = 'Signin to view this page!';
                $this->load->view('templates/header', $data);
                $this->load->view('charity/login_form', $data);
                $this->load->view('templates/footer', $data);
                return; 
            }
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');
		$this->form_validation->set_rules('location', 'Location', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');
		
		$data['title'] = "Edit News";

		$data['news_item'] = $this->news_model->get_news_where($slug);

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
       		$this->load->view('news/edit', $data);
       		$this->load->view('templates/footer', $data);
		}else{
			$this->news_model->save_news($slug);
			$this->load->view('templates/header', $data);
       		$this->load->view('news/success');
       		$this->load->view('templates/footer', $data);	 
		}
	}

	public function view($slug)
	{
		
		$data['news_item'] = $this->news_model->get_news_where($slug);
	
		$data['title'] = "News Item";
	

		$this->load->view('templates/header' , $data);
        $this->load->view('news/view', $data);
        $this->load->view('templates/footer', $data);

	}


    public function index()
	{
		
		$data['news'] = $this->news_model->get_news();
		$data['title'] = "Charity News";

		$this->load->view('templates/header', $data);
        $this->load->view('news/index', $data);
        $this->load->view('templates/footer', $data);

	}

	public function signup(){
		echo "<h3>NEED TO IMPLEMENT SIGNUP</h3>";
	}

	public function Login(){
		echo "<h3>NEED TO IMPLEMENT LOGIN</h3>";
	}


}
