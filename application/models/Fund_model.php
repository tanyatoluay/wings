<?php
class Fund_model extends CI_Model{

	public function __construct(){
		$this->load->database();
	}

	public function get_fund(){
		$query = $this->db->get('fundraising_goal');
		return $query->result_array();
	}

	public function get_fund_where($slug){
		$query = $this->db->get_where('fundraising_goal', array('slug' => $slug));
		return $query->row_array();
	}

	public function save_fund($slug){
		$data = array(
			'title' => $this->input->post('title'),
			'slug' => $slug,
			'date' => $this->input->post('date'),
			'goal' => $this->input->post('goal'),
			'text' => $this->input->post('text'));

		$this->db->where('slug', $slug);
		return $this->db->update('fundraising_goal', $data);
	}



	public function set_fund(){

		$slug = url_title($this->input->post('title'), 'dash', TRUE);

		$data = array(
			'title' => $this->input->post('title'),
			'slug' => $slug,
			'date' => $this->input->post('date'),
			'goal' => $this->input->post('goal'),
			'text' => $this->input->post('text'));


		return $this->db->insert('fundraising_goal', $data);		
	}

public function delete_fund($slug){
		$this->db->where("slug", $slug);
		$this->db->delete("fundraising_goal");

	}

}