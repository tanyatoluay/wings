<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Dashboard_model');
  }

  public function index()
  {
    $data['title'] = "Dashboard";
    $data['page_name'] = "Dashboard";
    $this->include_view('dashboard', $data);
  }
   public function categories(){

        $this->load->model('Dashboard_model');
      $data = $this->Dashboard_model->get_categories();

      print_r($data);
        }

}