<?php
class Fund extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('fund_model');
		$this->load->helper('url_helper');
		$this->load->helper('form');

	}

	public function create(){
				if(!isset($this->session->userdata['logged_in'])){
                $data['message_display'] = 'Signin to view this page!';
                $this->load->view('templates/header');
                $this->load->view('charity/login_form', $data);
                $this->load->view('templates/footer', $data);
                return; 
            }
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');
		$this->form_validation->set_rules('goal', 'Goal', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');

		$data['title'] = "Create Fundraising Goals";

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
       		$this->load->view('fund/create');
       		$this->load->view('templates/footer');
		}else{
			$this->fund_model->set_fund();
			$this->load->view('templates/header', $data);
			$this->load->view('fund/success');
			$this->load->view('templates/footer', $data);
		}
	}


	public function delete($slug){
				if(!isset($this->session->userdata['logged_in'])){
                $data['message_display'] = 'Signin to view this page!';
                $this->load->view('templates/header', $data);
                $this->load->view('charity/login_form', $data);
                $this->load->view('templates/footer');
                return; 
            }
		$this->fund_model->delete_fund($slug);
			$this->load->view('templates/header');
			$this->load->view('fund/success');
			$this->load->view('templates/footer');

	}


	public function edit($slug = NULL){
				if(!isset($this->session->userdata['logged_in'])){
                $data['message_display'] = 'Signin to view this page!';
                $this->load->view('templates/header', $data);
                $this->load->view('charity/login_form', $data);
                $this->load->view('templates/footer', $data);
                return; 
            }
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');
		$this->form_validation->set_rules('goal', 'Goal', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');
		
		$data['title'] = "Edit Fundraising Goals";

		$data['fund_item'] = $this->fund_model->get_fund_where($slug);

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
       		$this->load->view('fund/edit', $data);
       		$this->load->view('templates/footer', $data);
		}else{
			$this->fund_model->save_fund($slug);
			$this->load->view('templates/header', $data);
			$this->load->view('fund/success');
			$this->load->view('templates/footer');
		}
	}

	public function view($slug)
	{
		
		$data['fund_item'] = $this->fund_model->get_fund_where($slug);
	
		$data['title'] = "Fundraising Item";
	

		$this->load->view('templates/header', $data);
        $this->load->view('fund/view', $data);
        $this->load->view('templates/footer');

	}


    public function index()
	{
		
		$data['fund'] = $this->fund_model->get_fund();
		$data['title'] = "Fundraising Campaigns";

		$this->load->view('templates/header', $data);
        $this->load->view('fund/index', $data);
        $this->load->view('templates/footer');

	}

	public function signup(){
		echo "<h3>NEED TO IMPLEMENT SIGNUP</h3>";
	}

	public function Login(){
		echo "<h3>NEED TO IMPLEMENT LOGIN</h3>";
	}

}