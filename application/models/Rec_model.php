<?php
class Rec_model extends CI_Model{

	public function __construct(){
		$this->load->database();
	}

	public function get_rec(){
		$query = $this->db->get('recruitment_event');
		return $query->result_array();
	}

	public function get_rec_where($slug){
		$query = $this->db->get_where('recruitment_event', array('slug' => $slug));
		return $query->row_array();
	}

	public function save_rec($slug){
		$data = array(
			'title' => $this->input->post('title'),
			'slug' => $slug,
			'location' => $this->input->post('location'),			
			'date' => $this->input->post('date'),						
			'text' => $this->input->post('text'));

		$this->db->where('slug', $slug);
		return $this->db->update('recruitment_event', $data);
	}




	public function set_rec(){

		$slug = url_title($this->input->post('title'), 'dash', TRUE);

		$data = array(
			'title' => $this->input->post('title'),
			'slug' => $slug,
			'text' => $this->input->post('text'),
			'location' => $this->input->post('location'),			
			'date' => $this->input->post('date'));
		return $this->db->insert('recruitment_event', $data);
	}


public function delete_rec($slug){
		$this->db->where("slug", $slug);
		$this->db->delete("recruitment_event");

	}

}