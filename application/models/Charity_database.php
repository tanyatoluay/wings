<?php
class Charity_database extends CI_Model{

	public function __construct(){
		$this->load->database();
	}
	// Insert registration data in database
	public function registration_insert($data) {

		// Query to check whether username already exist or not
		$condition = "charity_name =" . "'" . $data['charity_name'] . "'";
		$this->db->select('*');
		$this->db->from('charity_organizations');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() == 0) {

		// Query to insert data in database
			$this->db->insert('charity_organizations', $data);
			if ($this->db->affected_rows() > 0) {
				return true;
			}
		} else {
			return false;
		}
	}

	// Read data using username and password
	public function login($data) {

		$condition = "charity_name =" . "'" . $data['charity_name'] . "' AND " . "password =" . "'" . $data['password'] . "'";
		$this->db->select('*');
		$this->db->from('charity_organizations');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	// Read data from database to show data in admin page
	public function read_user_information($charity_name) {

		$condition = "charity_name =" . "'" . $charity_name . "'";
		$this->db->select('*');
		$this->db->from('charity_organizations');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}

}