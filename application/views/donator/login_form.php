<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Wings</title>

  <link rel="stylesheet" href="https://www.studenti.famnit.upr.si/~89181002/wings/assets/css/demo1.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://www.studenti.famnit.upr.si/~89181002/wings/assets/css/header-basic-light.css" rel="stylesheet" type="text/css"/>
  <link href='https://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>


</head>

<body>



<!-- The content of your page would go here.  -->

<div class="menu">









<?php
if (isset($logout_message)) {
  echo "<div class='message'>";
  echo $logout_message;
  echo "</div>";
}
?>
<?php
if (isset($message_display)) {
  echo "<div class='message'>";
  echo $message_display;
  echo "</div>";
}
?>
<div id="main">
  <div id="login">
    <h2>Login Form for Donators</h2>
    <hr/>
    <?php echo form_open('donator/signin'); ?>
    <?php
    echo "<div class='error_msg'>";
    if (isset($error_message)) {
      echo $error_message;
    }
    echo validation_errors();
    echo "</div>";
    ?>
    <ul>
    <label>Username:</label>
    <input type="text" name="username" id="name" placeholder="username"/><br /><br />
    <label>Password:</label>
    <input type="password" name="password" id="password" placeholder="**********"/><br/><br />
    <input type="submit" value=" Login " name="submit"/><br />
    <div>
      <a href="https://www.studenti.famnit.upr.si/~89181002/wings/index.php/donator/show">Not a member yet? Join Us!</a>
        </ul>
    </div>
    <?php echo form_close(); ?>
  </div>
</div>




</div>
