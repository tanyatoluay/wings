<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Wings</title>

	<link rel="stylesheet" href="https://www.studenti.famnit.upr.si/~89181002/wings/assets/css/demo1.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://www.studenti.famnit.upr.si/~89181002/wings/assets/css/header-basic-light.css" rel="stylesheet" type="text/css"/>
	<link href='https://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>

</head>

<body>

<header class="header-basic-light">

	<div class="header-limiter">

		<h1><a href="https://www.studenti.famnit.upr.si/~89181002/wings/">Wings<span> for a better world</span></a></h1>

		<nav>
			<a href="https://www.studenti.famnit.upr.si/~89181002/wings/index.php/about">About</a>
			<a href="https://www.studenti.famnit.upr.si/~89181002/wings/index.php/news" class="selected">News</a>
			<a href="https://www.studenti.famnit.upr.si/~89181002/wings/index.php/fund">Fundraising</a>
			<a href="https://www.studenti.famnit.upr.si/~89181002/wings/index.php/rec">Recruitment</a>
			<a href="https://www.studenti.famnit.upr.si/~89181002/wings/index.php/charity/signin">Organizations</a>
			<a href="https://www.studenti.famnit.upr.si/~89181002/wings/index.php/donator/signin">Donators</a>
		</nav>
	</div>

</header>