<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Wings</title>

  <link rel="stylesheet" href="https://www.studenti.famnit.upr.si/~89181002/wings/assets/css/demo1.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://www.studenti.famnit.upr.si/~89181002/wings/assets/css/header-basic-light.css" rel="stylesheet" type="text/css"/>
  <link href='https://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>


</head>

<body>

<div class="menu">


<h2><?php echo $rec_item['title'];?></h2>
<p><?php echo $rec_item['text'];?></p>
<p><?php echo $rec_item['location'];?></p>
<p><?php echo $rec_item['date'];?></p>

<p><a href="<?php echo site_url('rec/edit/'.$rec_item['slug']); ?>">Edit</a></p>
<p><a href="<?php echo site_url('rec/delete/'.$rec_item['slug']); ?>">Delete</a></p>