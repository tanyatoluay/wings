<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Wings</title>

  <link rel="stylesheet" href="https://www.studenti.famnit.upr.si/~89181002/wings/assets/css/demo1.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://www.studenti.famnit.upr.si/~89181002/wings/assets/css/header-basic-light.css" rel="stylesheet" type="text/css"/>
  <link href='https://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>


</head>

<body>

<div class="menu">



<div id="main">
  <div id="login">
    <h2>Registration Form</h2>
    <hr/>
    <?php
    echo "<div class='error_msg'>";
      echo validation_errors();
    echo "</div>";
    echo form_open('donator/signup');

    echo form_label('Username: ');
    echo"<br/>";
    echo form_input('username');
    echo "<div class='error_msg'>";
    
    if (isset($message_display)) {
      echo $message_display;
    }
    echo"<br/>";
    echo form_label('E-mail: ');
    echo"<br/>";
    $data = array(
    'type' => 'email',
    'name' => 'email_value'
    );
    echo form_input($data);
    echo"<br/>";
    echo form_label('Location (City): ');
    echo"<br/>";
    $data = array(
    'type' => 'location',
    'name' => 'location_value'
    );
    echo form_input($data);
    echo"<br/>";
    echo form_label('Age: ');
    echo"<br/>";
    $data = array(
    'type' => 'age',
    'name' => 'age_value'
    );
    echo form_input($data);
    echo"<br/>";
    echo form_label('Phone Number: ');
    echo"<br/>";
    $data = array(
    'type' => 'phonenumber',
    'name' => 'phonenumber_value'
    );
    echo form_input($data);
    echo"<br/>";
    echo form_label('Password: ');
    echo"<br/>";
    echo form_password('password');
    echo"<br/>";
    echo"<br/>";
    echo form_submit('submit', 'Sign Up');
    echo form_close();
    ?>

    <ul><a href="https://www.studenti.famnit.upr.si/~89181002/wings/index.php/donator/signin">Are you already a member? Log in!</a></ul>
  </div>
</div>









