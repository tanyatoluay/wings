<?php
class Rec extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('rec_model');
		$this->load->helper('url_helper');
		$this->load->helper('form');

	}

	public function create(){
				if(!isset($this->session->userdata['logged_in'])){
                $data['message_display'] = 'Signin to view this page!';
                $this->load->view('templates/header');
                $this->load->view('charity/login_form', $data);
                $this->load->view('templates/footer');
                return; 
            }
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');
		$this->form_validation->set_rules('location', 'Location', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');

		$data['title'] = "Create Recruitment Event";

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
       		$this->load->view('rec/create');
       		$this->load->view('templates/footer');
		}else{
			$this->rec_model->set_rec();
			$this->load->view('templates/header', $data);
			$this->load->view('rec/success');
			$this->load->view('templates/footer');

		}
	}


		public function delete($slug){
					if(!isset($this->session->userdata['logged_in'])){
                $data['message_display'] = 'Signin to view this page!';
                $this->load->view('templates/header', $data);
                $this->load->view('charity/login_form', $data);
                $this->load->view('templates/footer');
                return; 
            }
		$this->rec_model->delete_rec($slug);
		$this->load->view('templates/header');
		$this->load->view('rec/success');
        $this->load->view('templates/footer');

	}


	public function edit($slug = NULL){
				if(!isset($this->session->userdata['logged_in'])){
                $data['message_display'] = 'Signin to view this page!';
                $this->load->view('templates/header', $data);
                $this->load->view('charity/login_form', $data);
                $this->load->view('templates/footer', $data);
                return; 
            }
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');
		$this->form_validation->set_rules('location', 'Location', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');
		
		$data['title'] = "Edit Recruitment Event";

		$data['rec_item'] = $this->rec_model->get_rec_where($slug);

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
       		$this->load->view('rec/edit', $data);
       		$this->load->view('templates/footer', $data);
		}else{
			$this->rec_model->save_rec($slug);
			$this->load->view('templates/header', $data);
			$this->load->view('rec/success');
			$this->load->view('templates/footer');

		}
	}

	public function view($slug)
	{
		
		$data['rec_item'] = $this->rec_model->get_rec_where($slug);
	
		$data['title'] = "Recrutiment Item";
	

		$this->load->view('templates/header', $data);
        $this->load->view('rec/view', $data);
        $this->load->view('templates/footer', $data);

	}


    public function index()
	{
		
		$data['rec'] = $this->rec_model->get_rec();
		$data['title'] = "Recrutiment Events";

		$this->load->view('templates/header', $data);
        $this->load->view('rec/index', $data);
        $this->load->view('templates/footer', $data);

	}

	public function signup(){
		echo "<h3>NEED TO IMPLEMENT SIGNUP</h3>";
	}

	public function Login(){
		echo "<h3>NEED TO IMPLEMENT LOGIN</h3>";
	}


}