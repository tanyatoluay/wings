<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wings</title>

    <link rel="stylesheet" href="https://www.studenti.famnit.upr.si/~89181002/wings/assets/css/demo1.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://www.studenti.famnit.upr.si/~89181002/wings/assets/css/header-basic-light.css" rel="stylesheet" type="text/css"/>
    <link href='https://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>


</head>

<body>



<!-- The content of your page would go here. -->



<div class="menu">


<h1>About Us</h1>


<div id="gallery-text">
    <div class="gallery-text">
        <h2> For Donators</h2>
        <p>
            <div>
By registering, donators can keep in touch with news, 
recruitment events and fundraising goals.
</div> 
<div>
All donators are welcome to use the information system regardless 
if they are donating money or not or about the topics they want to follow.
<ul><li><a href="https://www.studenti.famnit.upr.si/~89181002/wings/index.php/donator/signup">Register as a Donator</a></li></ul>


</div>
        </p>
    </div>
    <div class="gallery-text">
        <h2> For Charity Organizations</h2>
        <p>
            <div>
        By registering, organizations can create, edit and delete news, recruitment event and fundraising goals.
        </div> 
        <div>
        This way you can keep in touch with your community easily. 
        </div> 
        <div>
        All organizations are welcome to use the information system regardless of the topic they are aiming to improve. 
        </div>
        <div>
        The fundraising goals page is for organizations to inform people and, 
        it can be about different things (for example: book, clothes or food donation goals). 
<ul><li><a href="https://www.studenti.famnit.upr.si/~89181002/wings/index.php/charity/signup">Register as Organization</a></li></ul>


        </div>
        </p>
    </div>  
</div>













