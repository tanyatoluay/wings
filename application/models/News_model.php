<?php
class News_model extends CI_Model{

	public function __construct(){
		$this->load->database();
	}

	public function get_news(){
		$query = $this->db->get('charity_news');
		return $query->result_array();
	}

	public function get_news_where($slug){
		$query = $this->db->get_where('charity_news', array('slug' => $slug));
		return $query->row_array();
	}

	public function save_news($slug){
	##$slug = url_title($this->input->post('title'));
		$data = array(
				'title' => $this->input->post('title'),
				'slug' => $slug,
				'text' => $this->input->post('text'),
				'date' => $this->input->post('date'),
				'location' => $this->input->post('location'));

		$this->db->where("slug", $slug);	
		return $this->db->update('charity_news', $data);
	}


	public function set_news(){

		$slug = url_title($this->input->post('title'), 'dash', TRUE);

		$data = array(
			'title' => $this->input->post('title'),
			'slug' => $slug,
			'text' => $this->input->post('text'),
		    'date' => $this->input->post('date'),
			'location' => $this->input->post('location'));

		return $this->db->insert('charity_news', $data);
	}


	public function delete_news($slug){
		$this->db->where("slug", $slug);
		$this->db->delete("charity_news");

	}
}
