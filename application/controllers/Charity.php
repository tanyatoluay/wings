<?php
class Charity extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->helper('url_helper');

	// Load form helper library
		$this->load->helper('form');

	// Load form validation library
		$this->load->library('form_validation');

	// Load session library
		$this->load->library('session');

	// Load database
		$this->load->model('charity_database');

	}

// Show login page
	public function index() {
		$this->load->view('templates/header');
		$this->load->view('charity/login_form');
		$this->load->view('templates/footer');
	}

// Show registration page
	public function show() {
		$this->load->view('templates/header');
		$this->load->view('charity/registration_form');
		$this->load->view('templates/footer');
	}

// Validate and store registration data in database
	public function signup() {

	// Check validation for user input in SignUp form
		$this->form_validation->set_rules('charity_name', 'charity_name', 'trim|required');
		$this->form_validation->set_rules('email_value', 'email', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('charity/registration_form');
		} else {
			$data = array(
				'charity_name' => $this->input->post('charity_name'),
				'charity_email' => $this->input->post('email_value'),
				'charity_location' => $this->input->post('location_value'),
				'charity_bank_name' => $this->input->post('bank_name'),
				'charity_bank_account' => $this->input->post('bank_account'),
				'charity_phone_number' => $this->input->post('phone_value'),
				'password' => $this->input->post('password')
			);
			$result = $this->charity_database->registration_insert($data);
			if ($result == TRUE) {
				$data['message_display'] = 'Registration Successfully !';
				$this->load->view('templates/header');
				$this->load->view('charity/login_form', $data);
				$this->load->view('templates/footer');
			} else {
				$this->load->view('templates/header');
				$this->load->view('charity/registration_form');
				$this->load->view('templates/footer');
			}
		}
	}

	public function admin(){

		if(isset($this->session->userdata['logged_in'])){
			$data['charity_name'] = $this->session->userdata['logged_in']['charity_name'];
			$data['charity_email'] = $this->session->userdata['logged_in']['charity_email'];

			$this->load->view('templates/header');
			$this->load->view('charity/admin_page', $data);
			$this->load->view('templates/footer');
		}else{
			$data['message_display'] = 'Signin to view admin page!';
			$this->load->view('templates/header');
			$this->load->view('charity/login_form', $data);
			$this->load->view('templates/footer');
		}
	}

// Check for user login process
	public function signin() {

		$this->form_validation->set_rules('charity_name', 'Charity_name', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		$data = array(
			'charity_name' => $this->input->post('charity_name'),
			'password' => $this->input->post('password')
		);
		$result = $this->charity_database->login($data);
		if ($result == TRUE) {

			$charity_name = $this->input->post('charity_name');
			$result = $this->charity_database->read_user_information($charity_name);
			if ($result != false) {
				$session_data = array(
					'charity_name' => $result[0]->charity_name,
					'charity_email' => $result[0]->charity_email,
				);
			// Add user data in session
				$data = array('error_message' => 'Signin OK');
				$this->session->set_userdata('logged_in', $session_data);
				$this->load->view('templates/header',$data);
				$this->load->view('charity/login_form',$data);
			}
		} else {
			$data = array(
				'error_message' => 'Invalid Username or Password'
			);
			$this->load->view('templates/header');
			$this->load->view('charity/login_form', $data);
			$this->load->view('templates/footer');
		}
	}

// Logout from admin page
	public function logout() {

	// Removing session data
		$sess_array = array(
			'charity_name' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['message_display'] = 'Successfully Logout';
		$this->load->view('templates/header');
		$this->load->view('charity/login_form', $data);
		$this->load->view('templates/footer');
	}
}
