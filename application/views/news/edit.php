<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Wings</title>

  <link rel="stylesheet" href="https://www.studenti.famnit.upr.si/~89181002/wings/assets/css/demo1.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://www.studenti.famnit.upr.si/~89181002/wings/assets/css/header-basic-light.css" rel="stylesheet" type="text/css"/>
  <link href='https://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>


</head>

<body>

<div class="menu">






<h2>Edit the News</h2>




<?php echo validation_errors(); ?>

<?php echo form_open('news/edit/'.$news_item['slug']); ?>

    <label for="title">Title</label>
    <input type="input" name="title" value="<?php echo $news_item['title']; ?>" /><br />

    <label for="text">Location</label>
    <input type="input" name="location" value="<?php echo $news_item['location']; ?>" /><br />

    <label for="text">Date</label>
    <input type="input" name="date" value="<?php echo $news_item['date']; ?>" /><br />

    <label for="text">Text</label>
    <textarea name="text"><?php echo $news_item['text']; ?></textarea><br />


    <input type="submit" name="submit" value="Save news item" onClick="return confirm('Are you sure you want to edit?');" />

</form>